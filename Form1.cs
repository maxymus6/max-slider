﻿using System;
using System.Windows.Forms;

namespace sldiergame
{
    public partial class Form1 : Form
    {
        Grid Grid;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Grid = new Grid(3); //maybe grid can have settings
            Grid.CreateTiles(this);
            Grid.ShuffleTiles();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Grid.ShuffleTiles();
        }
    }
}
