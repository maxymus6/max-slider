﻿using System.Collections.Generic;

namespace sldiergame
{
    public class SettingsFactory
    {
        public Dictionary<Coordinate, string> GetSettings(Settings settings)
        {
            switch (settings)
            {
                case Settings.Example:
                    return Three();
                default:
                    return Three();
            }        
        }

        private Dictionary<Coordinate, string> Three()
        {
            return new Dictionary<Coordinate, string>()
            {
                { new Coordinate(0,0), "avatar-1.png"},
                { new Coordinate(0,1), "avatar.png"},
                { new Coordinate(0,2), "back.png"},
                { new Coordinate(1,0), "book.png"},
                { new Coordinate(1,1), "cancel.png"},
                { new Coordinate(1,2), "chat-1.png"},
                { new Coordinate(2,0), "chat-2.png"},
                { new Coordinate(2,1), "chat.png"},
                { new Coordinate(2,2), "copy.png"}
            };

         }
    }

    public struct Coordinate
    {
        public int x;
        public int y;

        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public enum Settings
    {
        Example
    }
}
