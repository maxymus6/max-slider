﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace sldiergame
{
    public class Grid
    {
        private Tile[,] _grid;
        private int _gridSize;

        public Grid(int size)
        {
            _grid = new Tile[size, size];
            _gridSize = size;
        }

        public Tile this[int x, int y]
        {
            get
            {
                return _grid[x, y];
            }
            set
            {
                _grid[x, y] = value;
            }
        }

        public void CreateTiles(Form1 form )
        {
            var settingsFactory = new SettingsFactory();
            var settings = settingsFactory.GetSettings(Settings.Example);

            int i = 1;
            for (int x = 0; x < _gridSize; x++)
            {
                for (int y = 0; y < _gridSize; y++)
                {
                    var coords = new Coordinate(x, y);
                    var tile = new Tile(i, coords);
                    this[x, y] = tile; 
                    
                    tile.picture = settings[coords];
                    var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\images\" + this[x, y].picture;
                    tile.Image = Image.FromFile(path);

                    tile.Click += ClickTileEvent;
                    form.Controls.Add(tile);

                    i++;
                }
            }

            this[2, 2].MakeEmptySquare();
        }

        private void ClickTileEvent(object clickedTile, EventArgs e)
        {
            var AdjacentTiles = ListOfAdjacentTiles( (Tile)clickedTile);
            var EmptySquareList = AdjacentTiles.Where(t => t.EmptySquare == true).ToList();

            if (EmptySquareList.Any())
            {
                var emptySquare = EmptySquareList.First();
                SwapTiles((Tile)clickedTile, emptySquare);
            }

            if ( UserHasWon())
            {
                MessageBox.Show("You win!!!");
            }

        }

        public List<Tile> ListOfAdjacentTiles(Tile tile)
        {
            var AdjacentTiles = new List<Tile>();

            var x = tile.Coordinate.x;
            var y = tile.Coordinate.y;

            if (x - 1 >= 0) // left 
            {
                AdjacentTiles.Add(_grid[x - 1, y]);
            }
            if (y - 1 >= 0) // up
            {
                AdjacentTiles.Add(_grid[x, y - 1]);
            }
            if (x + 1 < _gridSize) // right 
            {
                AdjacentTiles.Add(_grid[x + 1, y]);
            }
            if (y + 1 < _gridSize) // sa down 
            {
                AdjacentTiles.Add(_grid[x, y + 1]);
            }

            return AdjacentTiles;
        }

        private bool UserHasWon()
        {
            int i = 1;
            for (int x = 0; x < _gridSize; x++)
            {
                for (int y = 0; y < _gridSize; y++)
                {
                    if (_grid[x, y].Id != i)
                    {
                        return false;
                    }
                    i++;
                }
            }
            return true;
        }

        public void SwapTiles(Tile tile1, Tile tile2)
        {
            var tile1Location = tile1.Location;
            var tile2Location = tile2.Location;

            var tile1Coords = tile1.Coordinate;
            var tile2Coords = tile2.Coordinate;

            tile1.Location = tile2Location;
            tile1.Coordinate = new Coordinate(tile2Coords.x, tile2Coords.y);

            tile2.Location = tile1Location;
            tile2.Coordinate = new Coordinate(tile1Coords.x, tile1Coords.y);

            _grid[tile1.Coordinate.x, tile1.Coordinate.y] = tile1;
            _grid[tile2.Coordinate.x, tile2.Coordinate.y] = tile2;

        }

        public void ShuffleTiles()
        {
            for (int i = 0; i< 5; i++)
            {
                var emptyTile = GetEmptyTile();
                var adjacentTiles = ListOfAdjacentTiles(emptyTile);
                var randomLegalyMovableTile = adjacentTiles[new Random().Next(adjacentTiles.Count)];
                SwapTiles(emptyTile, randomLegalyMovableTile);
            }
        }

        public Tile GetEmptyTile()
        {
            for (int x = 0; x < _gridSize; x++)
            {
                for (int y = 0; y < _gridSize; y++)
                {
                    if (_grid[x, y].EmptySquare == true)
                    {
                        return _grid[x, y];
                    }
                }
            }

            throw new Exception("Theres not an empy tile on the board");
        }

        // http://stackoverflow.com/questions/2706500/how-do-i-generate-a-random-int-number-in-c
        public int GenerateRandom(int min, int max)
        {
            var seed = Convert.ToInt32(Regex.Match(Guid.NewGuid().ToString(), @"\d+").Value);
            return new Random(seed).Next(min, max);
        }


  
    }
}
