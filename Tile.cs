﻿using System.Drawing;
using System.Windows.Forms;

namespace sldiergame
{
    public partial class Tile : PictureBox
    {
        public int Id { get; set; }
        public string picture { get; set; }
        public bool EmptySquare { get; set; }
        public Coordinate Coordinate { get; set; }


        public Tile(int id, Coordinate coords) : base()
        {
            InitializeComponent();
            EmptySquare = false;
            Id = id;
            Coordinate = coords;
            Padding = new Padding(10);
            int tileWidth = 200;
            Location = new Point((Coordinate.x * tileWidth), (Coordinate.y * tileWidth));
            Size = new Size(tileWidth, tileWidth);
            SizeMode = PictureBoxSizeMode.StretchImage;
            Coordinate = coords;
        }

        public void MakeEmptySquare()
        {
            Image = null;
            BackColor = Color.Blue;
            EmptySquare = true;
        }
    }
}
